/**
 * Main.cpp
 * This is the entry point for the application
 */
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <SFML/Window.hpp>
#include <chrono>
#include "GameLoop.h"
#include "Window/Window.h"

using namespace std;
using namespace glm;

//GLint doesn't have different sizes on different compilers whereas int does
const GLint WIDTH = 1920, HEIGHT = 1080;

int main() {
	const Engine::WindowSettings myWindowSettings = { WIDTH, HEIGHT };
	Engine::Window myWindow = Engine::Window(myWindowSettings);
	myWindow.Show();

	//this will handle all the logic of our game
	GameLoop gameLoop(WIDTH, HEIGHT, &myWindow);
	//bind gameLoop's update function to the window
	myWindow.addLoopFunction([&gameLoop]() { gameLoop.update(); });
	myWindow.addInitFunction([&gameLoop]() { gameLoop.init(); });
	//this will start calling the gameLoop update and also polling events
	myWindow.startLoop();
	
	
	//
	// long frames = 0;
	// //remember the start time. just use an auto type
	// std::chrono::high_resolution_clock::time_point t_start = chrono::high_resolution_clock::now();
	//
	// while (window.isOpen()) {
	// 	frames++;
	// 	//get the current time
	// 	auto t_now = chrono::high_resolution_clock::now();
	// 	//get the durration in between the start time and now
	// 	double time = chrono::duration_cast<chrono::duration<double>>(t_now - t_start).count();//and this gets the time
	//
	// 	if (frames % 100 == 0) {
	// 		cout << frames / time << "\n";
	// 	}
	//
	// 	sf::Event windowEvent;
	// 	while (window.pollEvent(windowEvent)) {
	// 		//close the window if it is a close event
	// 		if (windowEvent.type == sf::Event::Closed)
	// 		{
	// 			window.close();
	// 			break;
	// 		}
	// 		gameLoop.addEvent(windowEvent);
	// 	}
	// 	gameLoop.update(time);
	//
	// 	window.display();
	// 	sf::sleep(sf::milliseconds(15));
	// }
	//
	// gameLoop.cleanUp();
	//
	//
	// window.close();

	return EXIT_SUCCESS;
}
